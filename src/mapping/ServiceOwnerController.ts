/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for managing all operations of the Service-Owner
 */

import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import OracleDBInteractionUtil          from "../utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import AppConfig                        from "../config/AppConfig";


 export default class ServiceOwnerController{
    private readonly _MAX_ARECORD_ABUSE = 100;
    /**
     * Create a sevice-owner
     * @param reqJSON (any) JSON request
     * @param generateID (boolean) Generate ID automatically or not
     */
    public async create(reqJSON:any, generateID:boolean){
        logger.info(`Creating service-owner entitiy in the data store`);
        logger.debug(`Request received for creating service-owner: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        var response!:string;
        try{
            response = await this.createServiceOwner(reqJSON,generateID);
            logger.info(`Service created sucessfully`);
        }catch(error){
            logger.error(`Error while creating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata){throw error;}
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }            
        }
        return response;
    }

    /**
     * Update service-owner entity. 
     * @param reqJSON (any) JSON request message
     */
    public async update(reqJSON:any){
        logger.info(`Updating service-owner entitiy in the data store`);
        logger.debug(`Request received for updating service-version: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        var response!:string;
        try{
            response = await this.updateServiceOwner(reqJSON);
            logger.info(`Service created sucessfully`);
        }catch(error){
            logger.error(`Error while creating service: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata){throw error;}
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }            
        }
        return response;
    }

    /**
     * Delete service owner record
     * @param srv_own_id (string) PK of the service owner
     */
    public async delete(srv_own_id:string){
        logger.info(`Deleting service-owner entitiy from the data store with ID ${srv_own_id}`);
        var response:string;
        try{
            let sql:string  = `DELETE FROM med_service_owner WHERE srv_own_id=:v0`;
            let binds       = [srv_own_id];
            let impacted = await OracleDBInteractionUtil.executeUpdate(sql,binds);
            if(impacted.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");   
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8253","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`Error received while performing DELETE DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);            
            if(error.metadata){throw error;}
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
        }

    }
    /**
     * Fetch all service owner records within from - to record numbers
     * @param from (number) From row number
     * @param to (number) to Row/record numbers
     */
    public async getAllOwners(from:number=1,to:number=10){
        logger.info(`Getting all service ownersfrom the db starting from ${from} - to - ${to}`);
        to = (to > this._MAX_ARECORD_ABUSE)?this._MAX_ARECORD_ABUSE:to;
        if(from > to){
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8250","Invalid input");
        }
        let sqlSel:string = `select RES.*,(SELECT COUNT(*) FROM med_service_owner) as TTL_ROWS from (
            SELECT ROWNUM,srv_own_id, srv_id, own_id, emailadd, escalationrank, created_on, updated_on, created_by, updated_by, last_ops_id FROM med_service_owner order by created_on desc
            ) RES where ROWNUM BETWEEN :v1 and :v2 order by rownum `;
        let bind = [from,to]
        try{
            let result = await OracleDBInteractionUtil.executeRead(sqlSel,bind);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.creatBusinessObjectForAll(result);
            return busObj
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)            
            if(error.metadata){
                throw error;
            }
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }                            
        }
    }
    
    /**
     * Get All Service owner using service_id
     * @param srv_id (string) Service ID
     */
    public async getAllByServiceID(srv_id:string){
        logger.info(`Getting all service owners from the db for a specific service: ${srv_id}`);        
        let sqlSel:string = `SELECT ROWNUM,srv_own_id, srv_id, own_id, emailadd, escalationrank, created_on, updated_on, created_by, updated_by, last_ops_id FROM med_service_owner WHERE srv_id = :v1 order by created_on desc`;
        let bind = [srv_id]
        try{
            let result = await OracleDBInteractionUtil.executeRead(sqlSel,bind);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.creatBusinessObjectForAll(result);
            return busObj
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)                        
            if(error.metadata){
                throw error;
            }
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }                                                    
        }
    }

    /**
     * Get all by service owner ID
     * @param srv_own_id (string) Service Owner ID
     */
    public async getAllByServiceOwnerID(own_id:string){
        logger.info(`Getting all service owners from the db for a specific service: ${own_id}`);        
        let sqlSel:string = `SELECT ROWNUM,srv_own_id, srv_id, own_id, emailadd, escalationrank, created_on, updated_on, created_by, updated_by, last_ops_id FROM med_service_owner WHERE own_id=:v1 order by created_on desc`;
        let bind = [own_id]
        try{
            let result = await OracleDBInteractionUtil.executeRead(sqlSel,bind);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.creatBusinessObjectForAll(result);
            return busObj
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)                        
            if(error.metadata){
                throw error;
            }
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }                            
        }        
    }

    /**
     * Get all by srv_own_id (primary key)
     * @param srv_own_id (string) Service Owner ID
     */
    public async getAllByServiceOwnerPKID(srv_own_id:string){
        logger.info(`Getting all service owners from the db for a specific service: ${srv_own_id}`);        
        let sqlSel:string = `SELECT ROWNUM,srv_own_id, srv_id, own_id, emailadd, escalationrank, created_on, updated_on, created_by, updated_by, last_ops_id FROM med_service_owner WHERE srv_own_id=:v1 order by created_on desc`;
        let bind = [srv_own_id]
        try{
            let result = await OracleDBInteractionUtil.executeRead(sqlSel,bind);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.creatBusinessObjectForAll(result);
            return busObj
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)                        
            if(error.metadata){
                throw error;
            }
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }                              
        }        
    }
    
    private async createServiceOwner(reqJSON:any, generateID:boolean){
        let srvOwnerObj = reqJSON['service-owner'];
        let srvOwnId:string = (generateID)? uuidv4(): srvOwnerObj.srv_own_id;        
        let sql:string = `INSERT INTO med_service_owner (srv_own_id,srv_id,own_id,emailadd,escalationrank,created_on,updated_on,created_by,updated_by,last_ops_id) VALUES (:v0,:v1,:v2,:v3,:v4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,:v5,:v6,:v7)`;
        let binds:any = [srvOwnId,srvOwnerObj.srv_id,srvOwnerObj.own_id,srvOwnerObj.email_add,srvOwnerObj.escalation_rank,'SYSTEM','SYSTEM',null];
        try{
            let selresult = await OracleDBInteractionUtil.executeRead(`SELECT srv_id from med_service where srv_id=:v1`, [srvOwnerObj.srv_id]);
            if(selresult.rows.length <= 0){            
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Invalid service id");
            }    
            let result = await OracleDBInteractionUtil.executeUpdate(sql,binds);
            if(generateID){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!", new PostRespBusinessObjects.Trace("srv_own_id",srvOwnId));
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
            }            
        }catch(error){
            logger.error(`Error received while performing DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);            
            if(error.metadata){throw error;}            
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8251","Error While performing the Operation");
            }
        }        
    }

    private async updateServiceOwner(reqJSON:any){
        let srvOwnerObj = reqJSON['service-owner'];
        let sql:string = `UPDATE med_service_owner SET  srv_id = COALESCE(:v1,srv_id),  own_id = COALESCE(:v2,own_id),  emailadd = COALESCE(:v3,emailadd),  escalationrank = COALESCE(:v4,escalationrank),  updated_on=CURRENT_TIMESTAMP,  updated_by = :v5,  last_ops_id=:v6 WHERE srv_own_id = :v7`;
        let binds:any = [srvOwnerObj.srv_id,srvOwnerObj.own_id,srvOwnerObj.email_add,srvOwnerObj.escalation_rank,'SYSTEM',null,srvOwnerObj.srv_own_id];
        try{
            let result = await OracleDBInteractionUtil.executeRead(`SELECT srv_own_id from med_service_owner where srv_own_id=:v1`,
                                                        [srvOwnerObj.srv_own_id]);
            if(result.rows.length <= 0){            
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");
            }                                            

            await OracleDBInteractionUtil.executeUpdate(sql,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");            
            
        }catch(error){
            logger.error(`Error received while performing DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);                        
            if(error.metadata){throw error;}
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8252","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8252","Error While performing the Operation");
            }
        }
    }

    private async creatBusinessObjectForAll(dbResult:any){
        return new Promise<any>((resolve:any,reject:any) => {
            if(dbResult.rows.length <= 0){                
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8253","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),                    
                    "trace": [ ]
                },
                "service-owner": []
            }; 
            /**
             * 
                        {
                            "source": "TotalRows",
                            "description": dbResult.rows[0].TTL_ROWS
                        }
                         */ 
            if(dbResult.rows[0].TTL_ROWS){
                bussObj.metadata["trace"].push({
                    "source": "TotalRows",
                    "description": dbResult.rows[0].TTL_ROWS
                });
            }
            for(let i:number =0; i< dbResult.rows.length; i++){
                bussObj["service-owner"].push({ 
                    "srv_own_id": dbResult.rows[i].SRV_OWN_ID ,
                    "srv_id":dbResult.rows[i].SRV_ID ,
                    "own_id": dbResult.rows[i].OWN_ID ,
                    "email_id": dbResult.rows[i].EMAILADD ,
                    "escalation_rank": dbResult.rows[i].ESCALATIONRANK ,
                    "created_on": dbResult.rows[i].CREATED_ON ,
                    "updated_on": dbResult.rows[i].UPDATED_ON ,
                    "created_by": dbResult.rows[i].CREATED_BY ,
                    "updated_by": dbResult.rows[i].UPDATED_BY ,
                    "last_ops_id": dbResult.rows[i].LAST_OPS_ID 
                });
            }
            resolve(bussObj);
        });
    }

    private getJSON(inp:any):any{
        try{
            let output = JSON.parse(inp);
            logger.debug(`Parsed JSON object: ${output}`);
            return output;
        }catch(error){
            return undefined;
        }
    }
 }


 /**
  * {
    "service-owner": {
        "srv_own_id": "cf5a359c-d467-4371-bf41-99ad73e00f25",
        "srv_id": "cf5a359c-d467-4371-bf41-99ad73e00f25",
        "own_id": "test123",
        "email_add": "test@email.com",
        "escalation_rank": 1        
    }
}    
  */