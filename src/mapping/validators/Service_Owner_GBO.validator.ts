import Joi from "joi";
import  logger                          from "../../utils/Logger";
import {PostRespBusinessObjects}        from "../bussObj/Response";

/**
 * Validator clss for validating ServiceOwner schema model
 */
export default class ServiceOwnerSchemaModel{
    ServiceOwnerReqSchema_CRT = Joi.object({
        "service-owner": {
            srv_own_id: Joi.string().min(8).max(56).required(),
            srv_id: Joi.string().min(8).max(56).required(),
            own_id: Joi.string().min(4).max(64).required(),
            email_add: Joi.string().email(),
            escalation_rank: Joi.number().integer().min(1).max(10)
        }        
    });

    ServiceOwnerReqSchema_UPD = Joi.object({
        "service-owner": {
            srv_own_id: Joi.string().min(8).max(56).required(),
            srv_id: Joi.string().min(8).max(56),
            own_id: Joi.string().min(4).max(64),
            email_add: Joi.string().email(),
            escalation_rank: Joi.number().integer().min(1).max(10)
        }        
    });

    
    /**
     * Validate the provided JSON object and provide the parsed schema for CREATE function
     * @example
     *  let sample:string = '{"service-owner":{"srv_id": "123456789"}}';
     *  try{
     *      let output = new ServiceOwnerSchemaModel().validate(JSON.parse(sample));
     *      // here output = parsed JSON object
     *  }catch(error){
     *      console.log(error);
     *      // error:  {"metadata":{"status":"8154","description":"Schema validation error","trace":[{"source":"schema-validation","description":"service-master.name is required"}]}}        
     *  }        
     * @param req (Object) JSON object containing valid ServiceMasterSchemaModel
     */
    public validateCreate(req:any){
        const {error,value} = this.ServiceOwnerReqSchema_CRT.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            const trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8154","Schema validation error",trace);                                    
        }else{            
            return value;
        }
    }

    public validateUpdate(req:any){
        const {error,value} = this.ServiceOwnerReqSchema_UPD.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            const trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8154","Schema validation error",trace);                                    
        }else{            
            return value;
        }
    }
}