/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                          from "express";
import {AppHealth}                      from "../config/AppConfig";
import logger                           from "../utils/Logger";
import {PostRespBusinessObjects}        from "../mapping/bussObj/Response";
import util                             from "util";
import ServiceOwnerSchemaModel          from "../mapping/validators/Service_Owner_GBO.validator";
import ServiceOwnerController           from "../mapping/ServiceOwnerController";



const router: any = express.Router();

/**
 * Create service-owner object in the specific table
 * @async
 */
router.post('/', async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for creating service-owner`);
    try{
        let parsedObj = new ServiceOwnerSchemaModel().validateCreate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller
            const resBO:string = await new ServiceOwnerController().create(parsedObj,request.query.generateid !== undefined);
            response.status(201);
            response.send(resBO);
        }        
    }catch(error){
        logger.debug(`Error occured while creating service-owner  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        response.send(error);
    }    
});

/**
 * Update Service-owner entity using srv_own_id
 */
router.put('/', async(request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for updating service-owner for id: ${request.params.id}`);        
    
    try{
        let parsedObj:any = new ServiceOwnerSchemaModel().validateUpdate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller
            const resBO:string = await new ServiceOwnerController().update(parsedObj);
            response.status(201);
            response.send(resBO);
        }        
    }catch(error){
        
    }
    response.status(501);
    response.send(`Update service-owner`);

});

/**
 * Delete specific entity from the database
 */
router.delete('/:id', async(request:any, response:any)=> {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for deleting service-owner for id: ${request.params.id}`);
    try{
        const resBO:string = await new ServiceOwnerController().delete(request.params.id);
        response.status(201);
        response.send(resBO);
    }catch(error){
        logger.debug(`Error occured while deleting specific service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        response.send(error);
    }    
});


/**
 * Get specific service-owner using srv_own_id
 * here:
 * id: Key to fetch data against
 * type: type of id i.e. using_srv_own_id, using_srv_id, using_own_id
 * @async
 */
router.get('/:type/:id', async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");        
    const type:string = request.params.type.toLowerCase();
    if(!request.params.id || !(type === 'using_srv_own_id' || type === 'using_srv_id' || type === 'using_own_id' )){
        response.status(500);
        response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8250","Invalid input"));
    }else{
        // Call service controller        
        try{
            const srvControler:ServiceOwnerController = new ServiceOwnerController();            
            var srvResp:any;
            switch(type){
                case 'using_own_id':
                    logger.debug(`Getting all service_owner info using owner_id: ${request.params.id}`);
                    srvResp = await srvControler.getAllByServiceOwnerID(request.params.id);
                    break;
                case 'using_srv_id':
                    logger.debug(`Getting all service_owner info using srv_id: ${request.params.id}`);
                    srvResp = await srvControler.getAllByServiceID(request.params.id);
                    break;
                case 'using_srv_own_id':
                    logger.debug(`Getting all service_owner info using service_owner_id: ${request.params.id}`);
                    srvResp = await srvControler.getAllByServiceOwnerPKID(request.params.id);
                    break;
            }
            // let srvResp = await srvControler.getAllOwners();
            response.status(201);
            response.send(srvResp);
        }catch(error){
            logger.debug(`Error occured while get ALL service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            response.status(500);
            response.send(error);
        }        
    }    
})

/**
 * Get all service_owners 
 * @async
 */
router.get('/', async (_:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for fetching service-owner using service owner ID`);    
    // Call service controller
    try{
        const srvControler:ServiceOwnerController = new ServiceOwnerController();
        let srvResp = await srvControler.getAllOwners();
        response.status(201);
        response.send(srvResp);
    }catch(error){
        logger.debug(`Error occured while get ALL service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        response.send(error);
    }        
    
})


export default router;